/**
 * Initialisation des composant de Materialize
 */
$(document).ready(function() {
    $('select').material_select();
    $('.modal').modal();
    $('.tooltipped').tooltip({delay: 50, html: true});
    $(".dropdown-button").dropdown();
  });

/**
 * Envoi les information de la carte première carte jouable
 */
$('#submittModal1').click(function(){
	$('#selectedPlayerField1').val($('#playerSelect1').val());
	$('#selectedCardField1').val($('#cardSelect1').val());
	$('#form1').submit();
	});
/**
 * Envoi les information de la carte seconde carte jouable
 */
$('#submittModal2').click(function(){
	$('#selectedPlayerField2').val($('#playerSelect2').val());
	$('#selectedCardField2').val($('#cardSelect2').val());
	$('#form2').submit();
	});

/**
 * Toast si ce n'est pas au joueur de jouer
 */
function toastNotPlayerTurn() {
	Materialize.toast('Ce n\'est pas votre tour', 5000);
}

function toastNotDraw() {
	Materialize.toast('Piochez une carte avant de jouer!',5000);
}

function toastIsDraw() {
	Materialize.toast('Vous avez déjà pioché!',5000);
}

/**
 * Tranforme les messages flash de session en toast.
 */
$('#divNotice > div').each(function() {
	Materialize.toast($(this).text(), 8000);
});