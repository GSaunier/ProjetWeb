// Script Parallax
$(document).ready(function() {
	$('.parallax').parallax();
});

/**
 * Tranforme les messages flash de session en toast.
 */
$('#divNotice > div').each(function() {
	Materialize.toast($(this).text(), 8000);
});