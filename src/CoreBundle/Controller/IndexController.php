<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CoreBundle\Entity\Lobby;

class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $repositoryGame = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LoveLetterBundle:Game')
        ;
        $games = $repositoryGame->findAll();
        
        $repositoryLobby = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        $lobbies = $repositoryLobby->findAll();
        
        return $this->render('CoreBundle::index.html.twig', [
            'games' => $games,
            'lobbies' => $lobbies
        ]);
    }
}
