<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
   /**
    * @ORM\OneToOne(targetEntity="LoveLetterBundle\Entity\Player")
    */
    private $player;
    
    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Lobby", inversedBy="users")
     */
    private $lobby;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function  __construct()
    {
        parent::__construct();
    }
    

    /**
     * Set player
     *
     * @param \LoveLetterBundle\Entity\Player $player
     *
     * @return User
     */
    public function setPlayer(\LoveLetterBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \LoveLetterBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set lobby
     *
     * @param \CoreBundle\Entity\Lobby $lobby
     *
     * @return User
     */
    public function setLobby(\CoreBundle\Entity\Lobby $lobby = null)
    {
        $this->lobby = $lobby;

        return $this;
    }

    /**
     * Get lobby
     *
     * @return \CoreBundle\Entity\Lobby
     */
    public function getLobby()
    {
        return $this->lobby;
    }
}
