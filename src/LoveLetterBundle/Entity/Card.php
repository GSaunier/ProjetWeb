<?php

namespace LoveLetterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Card
 *
 * @ORM\Table(name="card")
 * @ORM\Entity(repositoryClass="LoveLetterBundle\Repository\CardRepository")
 */
class Card
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPlayed", type="boolean")
     */
    private $isPlayed;

    /**
     * @var bool
     *
     * @ORM\Column(name="isVisible", type="boolean")
     */
    private $isVisible;
    
    /**
     * @ORM\ManyToOne(targetEntity="LoveLetterBundle\Entity\Game", inversedBy="cards")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;
    
    /**
     * @ORM\ManyToOne(targetEntity="LoveLetterBundle\Entity\Player")
     */
    private $player;
    
    /**
     * Constructor
     * @param int $type
     */
    public function __construct($type) {
        $this->type = $type;
        $this->isPlayed = false;
        $this->isVisible = false;
    }
 
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Card
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isPlayed
     *
     * @param boolean $isPlayed
     *
     * @return Card
     */
    public function setIsPlayed($isPlayed)
    {
        $this->isPlayed = $isPlayed;

        return $this;
    }

    /**
     * Get isPlayed
     *
     * @return bool
     */
    public function getIsPlayed()
    {
        return $this->isPlayed;
    }

    /**
     * Set isVisible
     *
     * @param boolean $isVisible
     *
     * @return Card
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * Get isVisible
     *
     * @return bool
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * Set game don't use !
     *
     * @param \LoveLetterBundle\Entity\Game $game
     *
     * @return Card
     */
    public function setGame(\LoveLetterBundle\Entity\Game $game)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \LoveLetterBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }
    
    /**
     * Set player
     *
     * @param \LoveLetterBundle\Entity\Player $player
     *
     * @return Card
     */
    public function setPlayer(\LoveLetterBundle\Entity\Player $player = null)
    {
        $this->player = $player;
        
        return $this;
    }
    
    /**
     * Get player
     *
     * @return \LoveLetterBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }
    
    /**
     * Get the id of player if the card has one
     * 
     * @return NULL|number
     */
    public function getPlayerId()
    {
        if($this->player == null)
            return null;
        else
            return $this->player->getId();
    }
    
    public function getName()
    {
        switch($this->type){
            case 1:
                return "Guard";
            case 2:
                return "Priest";
            case 3:
                return "Baron";
            case 4:
                return "Handmain";
            case 5:
                return "Prince";
            case 6:
                return "King";
            case 7:
                return "Countess";
            case 8:
                return "Princess";
            case 9:
                return "Card";
            case 10:
                return "Deck";
            default:
                return "Not a card";
        }
    }
    
    public function getEffect()
    {
        $str = "<div class=\"center\">".$this->type." - ".$this->getName()."</div><div align=\"left\">";
        switch($this->type){
            case 1:
                $str = $str. "Choisissez un joueur et essayez de deviner la</br>carte qu'il a en main (excepté le Guard), si vous</br>tombez juste, le joueur est éliminé de la manche.";
                break;
            case 2:
                $str = $str. "Regardez la main d'un autre joueur.";
                break;
            case 3:
                $str = $str. "Comparez votre carte avec celle d'un autre</br>joueur, celui qui a la carte avec la plus faible</br>valeur est éliminé de la manche.";
                break;
            case 4:
                $str = $str. "Jusqu'au prochain tour, vous êtes protégé</br>des effets des cartes des autres joueurs.";
                break;
            case 5:
                $str = $str. "Choisissez un joueur (y compris vous),</br>celui-ci défausse la carte qu'il a en</br>main pour en piocher une nouvelle.";
                break;
            case 6:
                $str = $str. "Échangez votre main avec un autre joueur de votre choix.";
                break;
            case 7:
                $str = $str. "Si vous avez cette carte en main en même</br>temps que le King ou le Prince, alors vous</br>devez défausser la carte de la Countess.";
                break;
            case 8:
                $str = $str. "Si vous défaussez cette carte, vous êtes éliminé de la manche.";
                break;
            case 9:
                $str = $str. "Une carte";
                break;
            case 10:
                $str = $str. "La pioche";
                break;
            default:
                $str = $str. "Not a card";
                break;
        }
        return $str."</div>";
    }
    
    public function getImageUrl()
    {
        $url = "images/Cards/";
        switch($this->type){
            case 1:
                $url = $url."1 - Guard.jpg";
                break;
            case 2:
                $url = $url."2 - Priest.jpg";
                break;
            case 3:
                $url = $url."3 - Baron.jpg";
                break;
            case 4:
                $url = $url."4 - Handmaid.jpg";
                break;
            case 5:
                $url = $url."5 - Prince.jpg";
                break;
            case 6:
                $url = $url."6 - King.jpg";
                break;
            case 7:
                $url = $url."7 - Countess.jpg";
                break;
            case 8:
                $url = $url."8 - Princess.jpg";
                break;
            case 9:
                $url = $url."Back.jpg";
                break;
            case 10:
                $url = $url."BackMulti.jpg";
            default:
                $url = $url."Not a card";
        }
        return $url;
    }
}
