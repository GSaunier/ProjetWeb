<?php

namespace LoveLetterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LoveLetterBundle\LoveLetterBundle;
use Doctrine\DBAL\Types\SmallIntType;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="LoveLetterBundle\Repository\GameRepository")
 */
class Game
{
    /******************************************************************************************************/
    /*                                                                                                    */
    /*                                          Les attributs                                             */
    /*                                                                                                    */
    /******************************************************************************************************/
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="isOver", type="boolean")
     */
    private $isOver;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="LoveLetterBundle\Entity\Card", mappedBy="game"
     * , orphanRemoval=true, cascade={"persist", "remove", "refresh"})
     */
    private $cards;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="LoveLetterBundle\Entity\Player", mappedBy="game"
     * , orphanRemoval=true, cascade={"persist", "remove", "refresh"})
     */
    private $players;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="LoveLetterBundle\Entity\History", mappedBy="game"
     * , orphanRemoval=true, cascade={"persist", "remove", "refresh"})
     */
    private $history;
    
    /**
     * @var array
     * 
     * @ORM\Column(name="playerOrder", type="array")
     */
    private $playerOrder;
    
    /**
     * @var SmallIntTypeint
     * 
     * @ORM\Column(name="currentPlayerInOrder", type="smallint", nullable=true)
     */
    private $currentPlayerInOrder;

    /**
     * 
     * @var boolean
     */
    private $emptyDeck;
    
    /**
     * @var int
     *
     * @ORM\Column(name="tokenMax", type="integer", nullable=true)
     */
    private $tokenMax;
    
    
    /******************************************************************************************************/
    /*                                                                                                    */
    /*                                       Les getter / setter                                          */
    /*                                                                                                    */
    /******************************************************************************************************/
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set isOver
     *
     * @param boolean $isOver
     *
     * @return Game
     */
    public function setIsOver($isOver)
    {
        $this->isOver = $isOver;
        
        return $this;
    }
    
    /**
     * Get isOver
     *
     * @return bool
     */
    public function getIsOver()
    {
        return $this->isOver;
    }
    
    /**
     * Add card
     *
     * @param \LoveLetterBundle\Entity\Card $card
     *
     * @return Game
     */
    public function addCard(\LoveLetterBundle\Entity\Card $card)
    {
        $this->cards[] = $card;
        $card->setGame($this);
        
        return $this;
    }
    
    /**
     * Remove card
     *
     * @param \LoveLetterBundle\Entity\Card $card
     */
    public function removeCard(\LoveLetterBundle\Entity\Card $card)
    {
        $this->cards->removeElement($card);
    }
    
    /**
     * Get cards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCards()
    {
        return $this->cards;
    }
    
    /**
     * Add player
     *
     * @param \LoveLetterBundle\Entity\Player $player
     *
     * @return Game
     */
    public function addPlayer(\LoveLetterBundle\Entity\Player $player)
    {
        $this->players[] = $player;
        $player->setGame($this);
        
        return $this;
    }
    
    /**
     * Remove player
     *
     * @param \LoveLetterBundle\Entity\Player $player
     */
    public function removePlayer(\LoveLetterBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
        $player->setGame($this);
    }
    
    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }
    
    /**
     * Add history
     *
     * @param \LoveLetterBundle\Entity\History $history
     *
     * @return Game
     */
    public function addHistory(\LoveLetterBundle\Entity\History $history)
    {
        $this->history[] = $history;
        
        return $this;
    }
    
    /**
     * Remove history
     *
     * @param \LoveLetterBundle\Entity\History $history
     */
    public function removeHistory(\LoveLetterBundle\Entity\History $history)
    {
        $this->history->removeElement($history);
    }
    
    /**
     * Get history
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * @return number la taille de l'historique
     */
    public function getHistorySize()
    {
        return $this->history->count();
    }
    
    /**
     * Set playerOrder
     *
     * @param array $playerOrder
     *
     * @return Game
     */
    public function setPlayerOrder($playerOrder)
    {
        $this->playerOrder = $playerOrder;
        return $this;
    }
    
    /**
     * Get playerOrder
     *
     * @return array
     */
    public function getPlayerOrder()
    {
        return $this->playerOrder;
    }
    
    /**
     * Set currentPlayerInOrder
     *
     * @param integer $currentPlayerInOrder
     *
     * @return Game
     */
    public function setCurrentPlayerInOrder($currentPlayerInOrder)
    {
        $this->currentPlayerInOrder = $currentPlayerInOrder;
        
        return $this;
    }
    
    /**
     * Get currentPlayerInOrder
     *
     * @return integer
     */
    public function getCurrentPlayerInOrder()
    {
        return $this->currentPlayerInOrder;
    }
    
    /**
     * @return boolean
     */
    public function isEmptyDeck()
    {
        return $this->emptyDeck;
    }

    /**
     * @param boolean $emptyDeck
     */
    public function setEmptyDeck($emptyDeck)
    {
        $this->emptyDeck = $emptyDeck;
    }
    
    /**
     * Set tokenMax
     *
     * @param integer $tokenMax
     *
     * @return Game
     */
    public function setTokenMax($tokenMax)
    {
        $this->tokenMax = $tokenMax;
    }
    
    /**
     * Get tokenMax
     *
     * @return integer
     */
    public function getTokenMax()
    {
        return $this->tokenMax;
    }
    
    /******************************************************************************************************/
    /*                                                                                                    */
    /*                                  Fonctions création de partie                                      */
    /*                                                                                                    */
    /******************************************************************************************************/
    
    /**
     * Constructeur
     */
    public function __construct()
    {
        $this->isOver = false;
        
        for ($i = 0; $i < 5; $i ++)
            $this->addCard(new Card(1));
        
        for ($i = 0; $i < 2; $i ++)
            $this->addCard(new Card(2));
        
        for ($i = 0; $i < 2; $i ++)
            $this->addCard(new Card(3));
        
        for ($i = 0; $i < 2; $i ++)
            $this->addCard(new Card(4));
        
        for ($i = 0; $i < 2; $i ++)
            $this->addCard(new Card(5));
        
        $this->addCard(new Card(6));
        $this->addCard(new Card(7));
        $this->addCard(new Card(8));
        
        $this->history[] = new History(1, $this, NEWGAME, null, null, null, null,null);
        
        $this->playerOrder = array();
    }
    
    /**
     * Prépare une manche (pioche, retrait des cartes)
     */
    public function setup()
    {
        //Tire une première carte face cachée
        $c = $this->getRandomCard();
        $c->setIsPlayed(true);
        //Tire deux autres cartes face visible si il y a deux joueurs
        if($this->players->count() < 3) {
            for ($i=0; $i<3; $i++) {
                $c = $this->getRandomCard();
                $c->setIsPlayed(true);
                $c->setIsVisible(true);
            }
        }
        // On fait piocher une carte à chaque joueur
        foreach ($this->players as $player) {
            $c = $this->getRandomCard();
            $c->setPlayer($player);
        }
        // Créer l'ordre si c'est une nouvelle partie
        if ($this->currentPlayerInOrder === null) {
            $this->createOrder();
            //Défini le nombre de token pour gagner
            if($this->getNbPlayer() == 2) {
                $this->tokenMax = 7;
            } else if($this->getNbPlayer() == 3) {
                $this->tokenMax = 5;
            } else {
                $this->tokenMax = 4;
            }
        }
    }
    
    /**
     * Création de l'ordre de jeu des joueurs
     */
    public function createOrder()
    {
        foreach ($this->players as $player) {
            $this->playerOrder[] = $player->getId();
        }
        shuffle($this->playerOrder);
        $this->setCurrentPlayerInOrder(0);
    }

    /**
     * Créer une nouvelle manche en réattribant les booleen des cartes
     */
    public function newGame()
    {
        // Réinitialise les cartes
        foreach ($this->cards as $card) {
            $card->setIsPlayed(false);
            $card->setIsVisible(false);
            $card->setPlayer();
        }
        
        // Réinitialise les protections
        foreach ($this->players as $player) {
            $player->setIsProtected(false);
            $player->setIsInRound(true);
        }
        
        $this->history[] = new History($this->history->count() + 1, $this, NEWROUND, null, null, null, null,null);
        // Le joueur actuel est le premier de l'ordre
        //$this->setCurrentPlayerInOrder($this->playerOrder[0]);
        $this->setup();
    }
    
    /******************************************************************************************************/
    /*                                                                                                    */
    /*                               Fonctions de consultation des joueurs                                */
    /*                                                                                                    */
    /******************************************************************************************************/
    
    /**
     * @return LoveLetterBundle\Entity\Player Le player qui doit jouer
     */
    public function getCurrentPlayer()
    {
        return $this->getPlayerById( $this->playerOrder[$this->currentPlayerInOrder] );
    }
    
    /**
     * @param number id du player actuel
     * @return number id du joueur assis en face
     */
    public function getOppositePlayer($playerId)
    {
        $tabOrder = $this->getPlayerOrder();
        if(count($this->getPlayers()) == 2) {
            if($tabOrder[0] == $playerId) {
                return $tabOrder[1];
            } else {
                return $tabOrder[0];
            }
        } else {
            $ordre = array_search($playerId, $tabOrder);
            if($ordre < 2) {
                return $tabOrder[$ordre + 2];
            } else {
                return $tabOrder[$ordre - 2];
            }
        }
    }
    
    /**
     * @param number id du player actuel
     * @return number id du joueur assis à gauche et qui joue après
     */
    public function getNextPlayer($playerId)
    {
        $tabOrder = $this->getPlayerOrder();
        $ordre = array_search($playerId, $tabOrder);
        if($ordre != $this->getNbPlayer()-1) {
            return $tabOrder[$ordre + 1];
        } else {
            return $tabOrder[0];
        }
    }
    
    /**
     * @param number id du player actuel
     * @return number id du joueur assis à droite et qui joue avant
     */
    public function getPreviousPlayer($playerId)
    {
        $tabOrder = $this->getPlayerOrder();
        $ordre = array_search($playerId, $tabOrder);
        if($ordre == 0) {
            return $tabOrder[$this->getNbPlayer() - 1];
        } else {
            return $tabOrder[$ordre - 1];
        }
    }
    
    /**
     * @return number Le nombre de joueur
     */
    public function getNbPlayer()
    {
        return count($this->getPlayers());
    }
    
    /**
     * @param int $playerId
     * @return LoveLetterBundle\Entity\Player Le joueur avec l'id correspondant
     */
    public function getPlayerById($playerId)
    {
        foreach ($this->getPlayers() as $player) {
            if ($player->getId() == $playerId)
                return $player;
        }
    }
    
    /**
     * Retourne true si le player est dans la partie.
     * @param \LoveLetterBundle\Entity\Player $player
     * @return boolean
     */
    public function isPlayerInGame(\LoveLetterBundle\Entity\Player $player)
    {
        return $this->players->contains($player);
    }
    
    /******************************************************************************************************/
    /*                                                                                                    */
    /*                                 Fonctions de consultation cartes                                   */
    /*                                                                                                    */
    /******************************************************************************************************/
    
    public function getCardById($CardId) {
        foreach ($this->getCards() as $card) {
            if($card->getId() == $CardId)
                return $card;
        }
    }
    
    /**
     * Retourne toutes les cartes d'un joueur
     * @param int $playerId
     * @return \Doctrine\Common\Collections\Collection[]
     */
    public function getPlayerCards($playerId)
    {
        $cards = array();
        foreach ($this->cards as $card) {
            if($card->getPlayer() != null && $card->getPlayer()->getId() == $playerId) {
                $cards[] = $card;
            }
        }
        return $cards;
    }
    
    /**
     * @param int $playerId
     * @return \LoveLetterBundle\Entity\Player La carte d'un joueur (qui n'est pas en train de jouer)
     */
    public function getPlayerCard($playerId)
    {
        foreach ($this->getCards() as $card) {
            if(!$card->getIsPlayed() && $card->getPlayer() != null && $card->getPlayer()->getId() == $playerId) {
                return $card;
            }
            
        }
    }
    
    /**
     * Retourne les cartes dans la main d'un joueur
     * @param int $playerId
     * @return \Doctrine\Common\Collections\Collection[]
     */
    public function getPlayerHand($playerId)
    {
        $cards= array();
        foreach ($this->cards as $card) {
            if ($card->getPlayer()!= null && $card->getPlayer()->getId() == $playerId && $card->getIsPlayed() === false) {
                $cards[] = $card;
            }
        }
        return $cards;
    }
    
    /**
     * @return array Tableau avec les carte du deck (dans deck) et les cartes retirée du jeu (dans removed)
     */
    public function getDeck()
    {
        $deck = array();
        $this->emptyDeck = true;
        foreach ($this->cards as $card)
        {
            if($card->getPlayer() == null) {
                if($card->getIsPlayed()) {
                    $deck["removed"][] = $card;
                } else  {
                    $deck["deck"][] = $card;
                    $this->emptyDeck = false;
                }
            }
        }
        return $deck;
    }
    
    /**
     * @param int $cardId l'id de la carte
     * @return boolean true si le joueur possède la carte
     */
    private function isPlayerCard($playerId, $cardId)
    {
        foreach ($this->getCards() as $card) {
            if($card->getId() == $cardId && $card->getPlayerId() == $playerId) {
                return true;
            }
        }
        return false;
    }
    
    /******************************************************************************************************/
    /*                                                                                                    */
    /*                                         Fonctions de jeu                                           */
    /*                                                                                                    */
    /******************************************************************************************************/
    
    /**
     * Fonction appellé dans le controleur quand un joueur pioche une carte
     * @param LoveLetterBundle\Entity\Player $playerId
     */
    public function drawControleur(\LoveLetterBundle\Entity\Player $player)
    {
        if ($this->getCurrentPlayer() == $player && !$this->getCurrentPlayer()->getIsDraw()) {
            $player->setIsDraw();
            $this->draw($player);
        }
    }
    
    /**
     * Fait piocher un joueur
     * @return LoveLetterBundle\Entity\Card
     */
    private function draw(Player $player)
    {
        $card = $this->getRandomCard();
        $card->setPlayer($player);
        $this->history[] = new History($this->history->count() + 1, $this, DRAW, $player->getId(), null, null, null,null);
    }
    
    /**
     * @return LoveLetterBundle\Entity\Card Une carte du deck
     */
    private function getRandomCard()
    {
        $deck = $this->cards->filter(function($card) {
            if($card->getPlayer() == null && !$card->getIsPlayed())
                return true;
        });
        $deck = $deck->toArray();
        return $deck[array_rand($deck)];
    }

    private function endRoundDeckEmpty()
    {
        $players = $this->getPlayers();
        $affectation = 0;
        $winId = array();
        foreach ($players as $p){
            if($p->getIsInRound()){
                $card = $this->getPlayerCard($p->getId());
                if($affectation < $card->getType()){
                    $affectation = $card->getType();
                    $winId = array();
                    $winId[] = $p->getId();
                } else if ($affectation == $card->getType()){
                    $winId[] = $p->getId();
                }
            }
        }
        foreach($winId as $win){
            $this->getPlayerById($win)->addScore();
            // Le joueur suivant est le gagnant
            $this->currentPlayerInOrder = array_search($win, $this->playerOrder);
        }
        $this->history[] = new History($this->history->count() + 1, $this, ENDDECK, null, null, null,$affectation,$winId);
        if(!$this->winner()){
            $this->newGame();
        } else {
            $score = $this->getPlayerById($winId[0])->getScore();
            $this->history[] = new History($this->history->count() + 1, $this, ENDGAME, null, null, null,$score,$winId);
        }
    }

    /**
     * Passe au joueur suivant dans l'ordre.
     */
    private function finishTurn()
    {
        // Test si il n'y a plus de carte dans le deck donc fin de manche
        $this->getDeck();
        if ($this->isEmptyDeck()) {
            $this->endRoundDeckEmpty();
        } else {
            // Sinon on continue à jouer dans cette manche
            // Trouve le prochain joueur non éliminé
            do {
                $this->currentPlayerInOrder = $this->currentPlayerInOrder + 1;
                if ($this->currentPlayerInOrder >= $this->getNbPlayer()) {
                    $this->currentPlayerInOrder = 0; // remet à zéro si hors tableau
                }
            } while ( ! $this->getCurrentPlayer()->getIsInRound() );
            // Enlève la protection
            $player = $this->getCurrentPlayer();
            if ($player->getIsProtected()) {
                $player->setIsProtected(false);
            }
        }

    }
    
    /**
     * Elimine un player de la manche et test si la manche est terminée
     * @param int $playerID L'id du player à éliminer
     */
    private function eliminatePlayer($playerID)
    {
        $player = $this->getPlayerById($playerID);
        $player->setIsInRound(false);
        $this->getPlayerCard($playerID)->setIsVisible(true);
        $this->history[] = new History($this->history->count() + 1, $this, ELIM, $playerID, null, null,null,null);
        
        //Test fin de partie
        $playerInRound = array();
        foreach ($this->players as $player) {
            if ($player->getIsInRound())
                $playerInRound[] = $player;
        }
        
        if (count($playerInRound) == 1) {
            $playerInRound[0]->addScore();
            // Le joueur suivant est le gagnant (-1 car on fait +1 dans finishTurn)
            $this->currentPlayerInOrder = array_search($playerInRound[0]->getId(), $this->playerOrder) - 1;
            $this->history[] = new History($this->history->count() + 1, $this, ENDROUND, $playerInRound[0]->getId(), null, null, null,null);
            if(!$this->winner()){
                $this->newGame();
            } else {
                $score = $playerInRound[0]->getScore();
                $winner[] = $playerInRound[0]->getId();
                $this->history[] = new History($this->history->count() + 1, $this, ENDGAME, null, null, null,$score,$winner);
            }
        }
        
    }
    
    /**
     * Permet de terminer la partie quand un joueur quitte.
     * @param \LoveLetterBundle\Entity\Player $player
     */
    public function playerLeave(\LoveLetterBundle\Entity\Player $player)
    {
        $this->isOver = true;
        $this->history[] = new History($this->history->count() + 1, $this, LEAVE, $player->getId(), null, null,null,null);
    }
    
    /**
     * Test si un joueur a atteint le score max
     * @return boolean True si un joueur a gagné la partie
     */
    public function winner(){
        foreach($this->players as $p){
            if($p->getScore() == $this->getTokenMax()){
                $this->isOver = true;
                return true;
            }
        }
        return false;
    }    

    /**
     * Fonction qui fait jouer une carte
     * @param int $playerID L'id du player qui joue la carte
     * @param int $cardID L'id de la carte jouée
     * @param int $playerTargetID L'id du player visé
     * @param int $selectedCard Le type de carte séléctonné (pour le Guard)
     * @return string[] Tableau d'informations à afficher au joueur
     */
    public function play($playerID, $cardID, $playerTargetID, $selectedCard)
    {
        $info = array();
        
        // Vérifie si le joueur possède bien la carte
        if(!$this->isPlayerCard($playerID, $cardID)) {
            $info[] = "Vous ne possèdez pas cette carte (tricheur) !";
            return $info;
        }

        // Vérifie si c'est au joueur de jouer
        if($this->getCurrentPlayer()->getId() != $playerID) {
            $info[] = "Ce n'est pas vous de jouer (tricheur) !";
            return $info;
        }
        
        // Vérifie que le joueur cible n'est pas protégé
        $playerTarget = $this->getPlayerById($playerTargetID);
        if($playerTarget != null && $playerTarget->getIsProtected()) {
            $info[] = "Ce joueur ne peut être pris pour cible car il est protégé (tricheur) !";
            return $info;
        }
        
        $card = $this->getCardById($cardID);
            
        // Vérifie si on doit jouer la Countess
        if ($card->getType() != 7) {
            $cards = $this->getPlayerHand($playerID);
            $c1 = $cards[0]->getType();
            $c2 = $cards[1]->getType();
            if ($c1 == 7 && ($c2 == 5 || $c2 == 6) || $c2 == 7 && ($c1 == 5 || $c1 == 6)) {
                $info[] = "Vous devez défausser la Comtesse !";
                return $info;
            }
        }
        
        // Marque la carte comme jouée
        $card->setIsPlayed(true);
        $card->setIsVisible(true);
        
        $player = $this->getPlayerById($playerID);
        
        
        // Si la carte ne peut pas avoir de cible donc pas d'effet
        if ($playerTargetID == -1) {
            $info[] = "Carte jouée sans effet.";
            $this->history[] = new History($this->history->count() + 1, $this, NOEFFECT, $playerID, null, $cardID, null,null);

        }else{
            if($card->getType() != 1 && $card->getType() != 3)
                $this->history[] = new History($this->history->count() + 1, $this, PLAY, $playerID, $playerTargetID, $cardID, null,null);
            
            switch ($card->getType()) {
                case 1:
                    $info[] = $this->playGuard($playerID, $playerTargetID,$cardID, $selectedCard);
                    break;
                case 2:
                    $info[] = $this->playPriest($playerTargetID);
                    break;
                case 3:
                    $info[] = $this->playBaron($playerTargetID, $playerID,$cardID);
                    break;
                case 4:
                    $info[] = $this->playHandmaid($playerID);
                    break;
                case 5:
                    $info[] = $this->playPrince($playerTargetID);
                    break;
                case 6:
                    $info[] = $this->playKing($playerID, $playerTargetID);
                    break;
                case 7:
                    $info[] = $this->playCountess();
                    break;
                case 8:
                    $info[] = $this->playPrincess($playerID);
                    break;
            }
        }
        
        $player->setIsDraw();
        // Passe au joueur suivant
        $this->finishTurn();
        return $info;
    }
    
    /******************************************************************************************************/
    /*                                                                                                    */
    /*                                    Fonctions action des carrtes                                    */
    /*                                                                                                    */
    /******************************************************************************************************/
    
    private function playGuard($playerID, $playerTargetID,$cardID, $selectedCard)
    {
        $oppositeUsername = $this->getPlayerById($playerTargetID)->getUser()->getUsername();
        if($this->getPlayerCard($playerTargetID)->getType() == $selectedCard) {
            $this->history[] = new History($this->history->count() + 1, $this, PLAYGUARDTRUE, $playerID, $playerTargetID, $cardID, $selectedCard,null);
            $this->eliminatePlayer($playerTargetID);            
            return $oppositeUsername." est éliminé.";
        } else {
            $this->history[] = new History($this->history->count() + 1, $this, PLAYGUARDFALSE, $playerID, $playerTargetID, $cardID, $selectedCard, null);
            return $oppositeUsername." n'a pas cette carte.";
        }
    }
    
    private function playPriest($playerTargetID)
    {
        $oppositePlayer = $this->getPlayerById($playerTargetID);
        return $oppositePlayer->getUser()->getUsername()." a la carte ".$this->getPlayerCard($playerTargetID)->getName();
    }
    
    private function playBaron($playerTargetID, $playerID, $cardID)
    {
        $cardTarget = $this->getPlayerCard($playerTargetID);
        $card = $this->getPlayerCard($playerID);
        $oppositeUsername = $this->getPlayerById($playerTargetID)->getUser()->getUsername();
        $typeArray = array();
        $typeArray[0] = $card->getType();
        $typeArray[1] = $cardTarget->getType();

        $this->history[] = new History($this->history->count() + 1, $this, PLAY, $playerID, $playerTargetID, $cardID, null,$typeArray);
        if ($cardTarget->getType() < $card->getType()) {
            $this->eliminatePlayer($playerTargetID);
            return $oppositeUsername." est éliminé car sa carte était ".$cardTarget->getName().".";
        } elseif ($cardTarget->getType() > $card->getType()) {
            $this->eliminatePlayer($playerID);
            return "Vous êtes éliminé car ".$oppositeUsername." a la carte ".$cardTarget->getName().".";
        } else {
            // Rien ne se passe (même carte)
            return "Vous avez la même carte.";
        }
    }
    
    private function playHandmaid($playerID)
    {
        $player = $this->getPlayerById($playerID);
        $player->setIsProtected(true);
        return "Vous êtes protégé pendant ce tour.";
    }
    
    private function playPrince($playerTargetId)
    {
        $oldCard = $this->getPlayerCard($playerTargetId);
        $oppositeUsername = $this->getPlayerById($playerTargetId)->getUser()->getUsername();
        $this->history[] = new History($this->history->count() + 1, $this, DISCARD, $playerTargetId,null, $oldCard->getId(), null, null);
        if($oldCard->getType() === 8){
            $this->eliminatePlayer($playerTargetId);
            return $oppositeUsername." est éliminé.";
        }else{
            $oldCard->setIsPlayed(true);
            $oldCard->setIsVisible(true);
            //Fait piocher une carte
            $player = $this->getPlayerById($playerTargetId);
            $this->getDeck();
            if(!$this->isEmptyDeck()){
                $this->draw($player);
            } else {
                $cards = $this->getCards();
                foreach($cards as $c){
                    if($c->getIsPlayed() && !$c->getIsVisible()){
                        $c->setPlayer($this->getPlayerById($playerTargetId));
                        $c->setIsPlayed(false);
                        $c->setIsVisible(false);
                        $this->history[] = new History($this->history->count() + 1, $this, DRAWEND, $playerTargetId, null, null, $c->getType(),null);
                    }
                }
            }
            return $oppositeUsername." a pioché une nouvelle carte.";
        }
    }
    
    private function playKing($playerID, $playerTargetID)
    {
        $player = $this->getPlayerById($playerID);
        $playerTarget = $this->getPlayerById($playerTargetID);
         
        $card = $this->getPlayerCard($playerID);
        $cardTarget = $this->getPlayerCard($playerTargetID);
        
        //On inverse les propriétaires
        $card->setPlayer($playerTarget);
        $cardTarget->setPlayer($player);
        return "Cartes échangées.";
    }
    
    private function playCountess()
    {
        return "Vous avez défaussé la comtesse.";
    }
    
    private function playPrincess($playerID)
    {
        $this->eliminatePlayer($playerID);
        return "Vous avez défaussé la Princesse, vous êtes éliminé.";
    }
}
