<?php

namespace LoveLetterBundle\Repository;

/**
 * HistoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HistoryRepository extends \Doctrine\ORM\EntityRepository
{
    public function getHistorySize($gameID)
    {        
        return $this->createQueryBuilder('h')
        ->select('COUNT(h)')
        ->where('h.game = :gameID')
        ->setParameter('gameID', $gameID)
        ->getQuery()
        ->getSingleScalarResult();
    }
}
